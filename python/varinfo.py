from pylab import *
from time import clock
import nibabel as nib
from math import factorial


def fmask(data,mask):
	"""
	fmask(data,mask)

	Input:
	data shape is (nx,ny,nz,...)
	mask shape is (nx,ny,nz)

	Output:
	out shape is (Nm,...)
	"""

	s = data.shape
	sm = mask.shape

	N = s[0]*s[1]*s[2]
	news = []
	news.append(N)

	if len(s) >3:
		news.extend(s[3:])

	tmp1 = np.reshape(data,news)
	fdata = tmp1.compress((mask > 0 ).ravel(),axis=0)

	return fdata.squeeze()

def probs(L,u):
	nk = u.shape[0];

	N = L.shape[0];
	P  =zeros(nk);
	edges = [.5]
	for k in range(nk):
	    P[k] = (L==u[k]).sum()/float(N);
	    edges.append(u[k]+.5)

	return P, array(edges)

def entropy(P):
	H = (-P*log(P)).sum()
	return H


def varinfo(r1,r2):
	"""
	Input:
	r1,r2 : integer valued classification vectors

	Output:

	list: (VI,NMI,Pjoin,NID)
	"""
	N = r1.shape[0]
	u1 = unique(r1)
	u2 = unique(r2)

	[P1,e1] = probs(r1,u1)
	[P2,e2] = probs(r2,u2)

	H1 = entropy(P1)
	H2 = entropy(P2)

	Pjoint = histogram2d(r1,r2,bins=(e1,e2))[0]/float(N)


	pij = P2[newaxis,:]*P1[:,newaxis]

	summe = Pjoint*log(Pjoint/pij)
	summe[Pjoint==0] = 0;

	MI = summe.sum()
	NMI = MI/(H1+H2)
	VI = H1 + H2 - 2*MI;
	NID = 1 - MI/(max(H1,H2))
	
	return VI,NMI,Pjoint,NID



def niftiVI(fn1,fn2):
	fnames = [fn1,fn2]
	nibims = map(nib.load,fnames)
	datas  = [im.get_data() for im in nibims]

	masks  = [ (dd != 0) for dd in datas]

	intmask = masks[0] & masks[1]


	C = [ fmask(dd,intmask) for dd in datas]

	VI = varinfo(C[0],C[1])

	return VI



