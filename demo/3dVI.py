import nibabel as nib
import numpy as np
from varinfo import varinfo, niftiVI
import sys




# fnames = sys.argv[1:3]

fnames=[
"E7329/meica.rest2e213/fitmod/k50.nii",
"E7329/meica.rest1e213/fitmod/k50.nii",
"E7279/meica.rest2e213/fitmod/k50.nii",
"E7279/meica.rest1e213/fitmod/k50.nii",
"E7326/meica.rest2e213/fitmod/k50.nii",
"E7326/meica.rest1e213/fitmod/k50.nii",
"E7280/meica.rest2e213/fitmod/k50.nii",
]

BASE="/misc/sfim/brenowitznd/fmri_data/b3T/"

fnames = map(lambda x : BASE+x,fnames)

N = len(fnames)

VI = np.zeros((N,N))

for i in range(N):
	for j in range(N):
		VI[i,j] = niftiVI(fnames[i],fnames[j])[1]
