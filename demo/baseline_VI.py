from pylab import *
from varinfo import *
from sklearn.metrics import adjusted_mutual_info_score as ami

n = 10000

ks = range(2,500,10)
ks.extend(  range(500,1000,50) )
# ks.extend( range(1000,2000,50) )
# ks.extend(range(2000,n+1,100))

ks = array(ks)

clustk = lambda k: random_integers(1,high=k,size=n)

VI = zeros(ks.shape)
NMI = zeros(ks.shape)
NID = zeros(ks.shape)
AMI = zeros(ks.shape)

count = 0
for kk in ks:
	
	print "Clustering %d of %d"%(kk,n)

	C1 = clustk(kk)
	C2 = clustk(kk)

	M = varinfo(C1,C2)

	tmpVI, tmpNMI = M[0:2]
	tmpNID = M[3]
	VI[count] =  tmpVI
	NMI[count] = tmpNMI
	NID[count] = tmpNID
	AMI[count] = ami(C1,C2)


	count += 1
	
semilogx(ks,VI)
xlabel('log(Clustering level k)')
ylabel('VI')
savefig('VI-baseline-semilogx.png')

figure()
plot(ks[:510],VI[:510])
xlabel('Clustering level k')
ylabel('VI')
xlim([0,ks[509]])
savefig('VI-baseline.png')