# Project Description

This is a mixed matlab and python project containing various codes for the clustering of fMRI data.

# Philosophy

Save the MATLAB linkage matrices to disk, then do analysis.

# Dependencies

## MATLAB

- [Munkres Assignment Algorithm](http://www.mathworks.com/matlabcentral/fileexchange/20328)
- [Matlab Statistics Toolbox](http://www.mathworks.com/help/toolbox/stats/crosstab.html)
- AFNI Matlab

## Python

- SciPy
- Numpy
- Pylab
- munkres (it's in PyPi, just do `easy_install munkres`)
- nibabel (this if from the nipy folks, `easy_install nibabel`)
- Pycluster (it's in PyPi, just do `easy_intall Pycluster`)

#File List

## Python

- `cluster.py`: Contains many functions, but command line usage will perform color matching using the hungarian algorithm

		python -m cluster <base clustering> <another clustering>

- `varinfo.py`: Calculates the VI and other information theoretic stuff. Import it as a module, take a look at docstring of varinfo. See demo `baseline_VI.py` for example usage


## MATLAB

- `varinfo.m` : Calculates variation of information (including mutual information, entropy, etc.). Uses matlab function `crosstab` which is much faster than for loops. Therefore, this function is much faster than any equivalents I have found.  
- `savelinkage.m` : useful function to save MATLAB linkage matrix given a  Brik and a mask filename.
- `afni_linkage.m` : given mask filename and volume filename will generate the linkage matrix
- `clusthung.m` : implements the hungarian algorithm. loads the filenames of two afni datasets and colormatches the second to the first. Requires file `munkres.m`. look for it in the matlab file exchange.
- `viMC.m` : uses a monte carlo procedure to estimate the baseline VI for various values of k.
- `saveclustns.m` : Prantik's method for rearranging the output of `cluster(k,'maxclust')` so that the nodes have a color scheme that makes sense.
- 'silmaps.m' : Calculates the silhouette value map for a given dataset and its clustering.


# Development Notes:

## `dendrogram_stabmap.m`

- Recursion in matlab does not work, should use while loop instead
- Stability map from bottom leafs
- Rescaling for display purposes

## `varinfo.m`

- After some testing, I changed this file on 2012-12-14. It should work now. 

## `varinfo.py`

- I tested this file against `varinfo.m`, and it should work. The usage is the same as matlab.

