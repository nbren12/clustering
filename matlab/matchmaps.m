clear all
close all
clc

%load images

name1= 'rest1_maps+orig';
name2 = 'rest2_maps+orig';
mask1 = 'rest1_mask.nii.gz';
mask2 = 'rest2_mask.nii.gz';


im1 = BrikLoad(name1,'vector');
im2 = BrikLoad(name2,'vector');
m1  = BrikLoad(mask1,'vector'); m1 = (m1==0);
m2  = BrikLoad(mask2,'vector'); m2 = (m2==0);
m = m1 & m2;

%mask the data
im1(m,:) = [];
im2(m,:) = [];


%construct cost matrix
[~, n1] = size(im1);
[~, n2] = size(im2);

C = corr(im1,im2);
C = 1 -C;


order = munkres(C);
costs = [];
for i = 1:length(order)
    costs =[ costs, C(i,order(i))];
end

pairs = [(1:length(order))'-1,order'-1,costs(:)];
[~,inds] = sort(costs);

sort_pairs = pairs(inds,:);

f= fopen('order.txt','w');
for i =1:length(order);
    if i < length(order)
    fprintf(f,'%d,',order(i)-1);
    else
        fprintf(f,'%d',order(i)-1);
    end

end
fclose(f);





