% File names
epiname1   = 'rest1_good_cbk+orig';
maskname1  = 'mask1+orig';
outprefix2 = 'rest1_good';

epiname2   = 'rest2_good_cbk+orig';
maskname2  = 'mask2+orig';
outprefix2 = 'rest2_good';

% File names
% epiname1   = 'rest1_mmix_cbk+orig';
% maskname1  = 'mask1.ribbon+orig';
% outprefix1 = 'rest1_mmix';
% 
% epiname2   = 'rest2_mmix_cbk+orig';
% maskname2  = 'mask2.ribbon+orig';
% outprefix2 = 'rest2_mmix';

Opt.OverWrite = 'y';

% Load the data
V1 = BrikLoad(epiname1,'vector');
[~,info] = BrikInfo(epiname1);
m1    = BrikLoad(maskname1,'vector'); %m1 = std(V1,[],2) > eps;

V2 = BrikLoad(epiname2,'vector');
m2   = BrikLoad(maskname2,'vector'); %m2 = std(V2,[],2) > eps;

[n nc] = size(V1);



% Mask the data
M = (m1~=0)&(m2~=0);

mv1 = V1(M,:);
mv2 = V2(M,:);

%transform mv2 to mv1
% mv2*T ~ mv1, T~mv2'*mv1
T = mv2 \ mv1;

mv2_tilde = mv2*T;

% Output mv2
out = zeros(n,nc);
out(M,:) = mv2_tilde;

Opt.Prefix = [outprefix2 '_T'];
WriteBrik(out,info,Opt);


%Get residual voxelwise
resid = mv1-mv2_tilde;
ssr = sqrt(sum(resid.^2,2));

%get corr voxelwise
znorm1 = mv1 - repmat(mean(mv1,2),[1 nc]); 
znorm1 = znorm1./repmat(std(znorm1,[],2)*sqrt(nc),[1 nc]);

znorm2 = mv2_tilde - repmat(mean(mv2_tilde,2),[1 nc]); 
znorm2 = znorm2./repmat(std(znorm2,[],2)*sqrt(nc),[1 nc]);


r = sum(znorm1.*znorm2,2);

%output ssr
out = zeros(n,1);
out(M,:) = ssr;

outinfo.DATASET_RANK = [3 1];
outinfo.DATASET_DIMENSIONS = info.DATASET_DIMENSIONS;
outinfo.TYPESTRING = info.TYPESTRING;
outinfo.SCENE_DATA = info.SCENE_DATA;
outinfo.ORIENT_SPECIFIC = info.ORIENT_SPECIFIC;
outinfo.ORIGIN = info.ORIGIN;
outinfo.DELTA = info.DELTA;
outinfo.BRICK_LABS = 'ssr';

Opt.Scale = 1;
Opt.Prefix = [outprefix2 '_g7'];
WriteBrik(out,outinfo,Opt);

%output r
out = zeros(n,1);
out(M,:) = r;

outinfo.DATASET_RANK = [3 1];
outinfo.DATASET_DIMENSIONS = info.DATASET_DIMENSIONS;
outinfo.TYPESTRING = info.TYPESTRING;
outinfo.SCENE_DATA = info.SCENE_DATA;
outinfo.ORIENT_SPECIFIC = info.ORIENT_SPECIFIC;
outinfo.ORIGIN = info.ORIGIN;
outinfo.DELTA = info.DELTA;
outinfo.BRICK_LABS = 'corr';

Opt.Scale = 1;
Opt.Prefix = [outprefix2 '_g2'];
WriteBrik(out,outinfo,Opt);




