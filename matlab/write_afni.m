function [err] = write_afni(data,name,template)

[e,I]=BrikInfo(template);
opt.OverWrite='y'
opt.Prefix=name
[err,ErrMessage,Info]=WriteBrik(data,I,opt);