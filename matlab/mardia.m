function k = mardia(X)

[m n] = size(X);
mu = mean(X);
sig = cov(X);

Xdm = X - repmat(mu,[m 1]);

eme = sum(Xdm.*(sig \ Xdm')',2);

k = mean(eme.^2);