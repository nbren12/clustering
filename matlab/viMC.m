clear all
close all
clc

N = 22000;
n = 2;
ks = [2:50,75,100:50:2000];
nk = length(ks);
VIs = zeros(n,length(ks));
muVI = zeros(length(ks),1);
for k = 1:nk
display(ks(k));
    r1 = floor(ks(k)*rand(N,n)+1);
    r2 = floor(ks(k)*rand(N,n)+1);
    
    
for i = 1:n
    

VIs(i,k) = varinfo(r1(:,i),r2(:,i));



end

end

muVI  = mean(VIs,1);
sigVI = std(VIs,[],1)/sqrt(n);
plot(ks,2*log(ks),'r');
hold on;
errorbar(ks,muVI,sigVI);

