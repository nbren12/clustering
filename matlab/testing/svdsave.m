function svdsave(brikname,mname,prefix)


% Load the data
data = BrikLoad(brikname,'vector');
m    = BrikLoad(mname,'vector');
[~,info] = BrikInfo(brikname); 
[N, Nt] = size(data);

% Mask the data
m = (m==0);
data(m,:) = [];

% Variance normalize across time
dataznorm = data - repmat(mean(data,2),[1 Nt]);
dataznorm = dataznorm./ (repmat(std(dataznorm,1,2)*sqrt(Nt),[1,Nt]));

% Compute the SVD
[U,S,V] = svd(dataznorm,'econ');

% Output the singular vectors
out = zeros(length(m),Nt);

out(~m,:) = U;
Opt.Prefix = prefix;
WriteBrik(out,info,Opt);

