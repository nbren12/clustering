clear all
close all
clc

%load images
name1= 'rest1_US_21+orig';
mask1 = 'mask1.ribbon+orig';

%atlas = 'rest2_mask.nii.gz';


[im info]  = BrikLoad(name1,'vector');
m = BrikLoad(mask1,'vector'); m = (m~=0);
[N NT] = size(im);

%mask the data
mim = im(m,:);


%fit gaussian mixture model
Options = statset('MaxIter',500,'Display','final');
gm = gmdistribution.fit(mim,20,'Options',Options);
P = gm.posterior(mim);
clusts = gm.cluster(mim);
Psil = sort(P,2,'descend');
s1 = Psil(:,1);
s2 = Psil(:,2);
Psil = (s1-s2)*2./(s1+s2);

% Output the remapped

outinfo.DATASET_RANK = [3 3];
outinfo.DATASET_DIMENSIONS = info.DATASET_DIMENSIONS;
outinfo.TYPESTRING = info.TYPESTRING;
outinfo.SCENE_DATA = info.SCENE_DATA;
outinfo.ORIENT_SPECIFIC = info.ORIENT_SPECIFIC;
outinfo.ORIGIN = info.ORIGIN;
outinfo.DELTA = info.DELTA;
outinfo.BRICK_LABS = 'clusts~pprob~psil';



out = zeros(N,3);

out(m,1) = clusts;
out(m,2) = max(P,[],2);
out(m,3) = Psil;
Opt.Prefix = 'gm';
Opt.Scale = 1;
Opt.OverWrite = 'y';
WriteBrik(out,outinfo,Opt);


