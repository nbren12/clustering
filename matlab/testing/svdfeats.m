clear all 
close all
clc

epiname = 'fitts+orig';
mname = '../../rest34.mask+orig';

[data,info] = BrikLoad(epiname,'vector');
[M,minfo] = BrikLoad(mname,'vector');
mmix = load('melodic_mix');

M = M ~=0;

md  = data(M,:);


[U,S,V] = svd(md,'econ');



nx = info.DATASET_DIMENSIONS(1);
ny = info.DATASET_DIMENSIONS(2);
nz = info.DATASET_DIMENSIONS(3);
nt = info.DATASET_RANK(2);
nc = 33;

out = zeros(nx*ny*nz,nc);
out(M,:) = U(:,1:33);


outinfo.DATASET_RANK = [3 nc];
outinfo.DATASET_DIMENSIONS = info.DATASET_DIMENSIONS;
outinfo.TYPESTRING = info.TYPESTRING;
outinfo.SCENE_DATA = info.SCENE_DATA;
outinfo.ORIENT_SPECIFIC = info.ORIENT_SPECIFIC;
outinfo.ORIGIN = info.ORIGIN;
outinfo.DELTA = info.DELTA;
outinfo.BRICK_LABS = '';



Opt.Prefix = 'orthfeats';
Opt.Scale = 1;
Opt.OverWrite = 'y';
WriteBrik(out,outinfo,Opt);

system(['3drefit -redo_bstat -newid ',Opt.Prefix,'+orig']);