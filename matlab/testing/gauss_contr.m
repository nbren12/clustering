clear all
close all
clc

%load images
name1= 'rest1_U_21+orig';
mask1 = 'mask1+orig';
tempname = 'fo_clust+orig';

%atlas = 'rest2_mask.nii.gz';


[im info]  = BrikLoad(name1,'vector');
m = BrikLoad(mask1,'vector'); m = (m~=0);
template = BrikLoad(tempname,'vector'); obj = (template==3);
face = temp 

[N NT] = size(im);
Nm = sum(m);
%mask the data
mim = im(m,:);

mtemp = temp(m,:);

ROI = im(temp,:);
%construct starting stuff
mu = mean(mim(mtemp,:),1);
Sigma(:,:,1) = cov(mim(mtemp,:));
dthresh = 36;
converged = false;






% Output the remapped

outinfo.DATASET_RANK = [3 100];
outinfo.DATASET_DIMENSIONS = info.DATASET_DIMENSIONS;
outinfo.TYPESTRING = info.TYPESTRING;
outinfo.SCENE_DATA = info.SCENE_DATA;
outinfo.ORIENT_SPECIFIC = info.ORIENT_SPECIFIC;
outinfo.ORIGIN = info.ORIGIN;
outinfo.DELTA = info.DELTA;
%outinfo.BRICK_LABS = 'd~mask';



Opt.Prefix = 'gauss_contr';
Opt.Scale = 1;
Opt.OverWrite = 'y';
WriteBrik(out,outinfo,Opt);


