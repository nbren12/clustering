clear all
close all
clc

%load images
name1= 'rest1_U_21+orig';
mask1 = 'mask1+orig';
tempname = 'ffa_ROI+orig';

%atlas = 'rest2_mask.nii.gz';


[im info]  = BrikLoad(name1,'vector');
m = BrikLoad(mask1,'vector'); m = (m~=0);
template = BrikLoad(tempname,'vector'); ffa = (abs(template-1)<.05);

[N NT] = size(im);
Nm = sum(m);

%mask the data
mim = im(m,:);
ffam = ffa(m,:);
aic = [];
bic = [];
nlogl = [];

for i = 43:100
fprintf ('GM K %d\n',i);
%fit gaussian mixture model
Options = statset('MaxIter',200);
try 
gm = gmdistribution.fit(mim,i,'Options',Options,'Regularize',eps);
catch
    
    display('woops');
end
%P = gm.posterior(mim);
%clusts = gm.cluster(mim);
%Psil = sort(P,2,'descend');
%s1 = Psil(:,1);
%s2 = Psil(:,2);
%Psil = (s1-s2)*2./(s1+s2);
aic = [aic, gm.AIC];
bic = [bic, gm.BIC];
nlogl = [nlogl, gm.NlogL];
end
% Output the remapped
% 
% outinfo.DATASET_RANK = [3 3];
% outinfo.DATASET_DIMENSIONS = info.DATASET_DIMENSIONS;
% outinfo.TYPESTRING = info.TYPESTRING;
% outinfo.SCENE_DATA = info.SCENE_DATA;
% outinfo.ORIENT_SPECIFIC = info.ORIENT_SPECIFIC;
% outinfo.ORIGIN = info.ORIGIN;
% outinfo.DELTA = info.DELTA;
% outinfo.BRICK_LABS = 'clusts~pprob~psil';
% 
% 
% 
% out = zeros(N,3);
% 
% out(m&ffa,1) = clusts;
% out(m&ffa,2) = max(P,[],2);
% out(m&ffa,3) = Psil;
% Opt.Prefix = 'split_ffa_k2';
% Opt.Scale = 1;
% Opt.OverWrite = 'y';
% WriteBrik(out,outinfo,Opt);


