function [L, mask1,inds,cumcost] = dendrogram_stabmap(varargin)
% Recursion does not work, do while loops instead

if nargin > 0
    link1fn = varargin{1};
    mask1fn = varargin{2};
else
    link1fn = ['linkages/','m15.rest12.linkage.mat'];
    mask1fn = ['./','rest12.mask+orig'];
end

L = load(link1fn);
L = L.l;



k = 2000;

[mask1,info] = BrikLoad(mask1fn,'vector');

N = sum(mask1);

cl1 = cluster(L,'maxclust',2000);
cumcost = flipud(cumsum(flipud(L(:,3))));

inds = arrayfun(@(x)(get_ind(x,L)),1:N);
hts = cumcost(inds);
hts= hts(:);

hts = (hts-min(hts))/(max(hts)-min(hts));

% Output the remapped
out = unmask(hts,mask1);

outinfo.DATASET_RANK = [3 1];
outinfo.DATASET_DIMENSIONS = info.DATASET_DIMENSIONS;
outinfo.TYPESTRING = info.TYPESTRING;
outinfo.SCENE_DATA = info.SCENE_DATA;
outinfo.ORIENT_SPECIFIC = info.ORIENT_SPECIFIC;
outinfo.ORIGIN = info.ORIGIN;
outinfo.DELTA = info.DELTA;


Opt.Prefix = 'stabmap';
Opt.Scale = 1;
Opt.OverWrite = 'y';
WriteBrik(out,outinfo,Opt);
system('3drefit -redo_bstat stabmap+orig');







end


function ind = get_ind(lab,L)

col1 = L(:,1) ==lab;
col2 = L(:,2) ==lab;
ind = find(col1 | col2);

end



function out = unmask(data,mask)

out = zeros(length(mask),size(data,2));
out(mask~=0,:) = data;


end
