clear all
close all
clc

%load images

name1= 'rest1_denoised+orig';
%name2 = 'rest2_maps+orig';
mask1 = 'mask+orig';
%mask2 = 'rest2_mask.nii.gz';

im1 = BrikLoad(name1,'vector');
%im2 = BrikLoad(name2,'vector');
m1  = BrikLoad(mask1,'vector'); m1 = (m1==0);
%m2  = BrikLoad(mask2,'vector'); m2 = (m2==0);
%m = m1 & m2;
m = m1;

%mask the data
im1(m,:) = [];
%im2(m,:) = [];


%load melodic mix
load melodic_mix; load accepted.txt; load rejected.txt;
accepted = accepted+1; rejected = rejected +1;
Xg = melodic_mix(:,accepted); Xb = melodic_mix(:,rejected);
X = [Xg zeros(size(Xb))]*pinv([Xg Xb]);


imznorm = im1 - repmat(mean(im1,2),[1 240]);
imznorm = imznorm./ (repmat(std(imznorm,1,2)*sqrt(240),[1,240]));
[~,info] = BrikInfo(name1);



[U,S,V] = svd(imznorm,'econ');

out = zeros(length(m),240);
out(~m,:) = U;

Opt.Prefix = 'rest1_PC_mlab';
%WriteBrik(out,info,Opt);




