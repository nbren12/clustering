clear all
close all
clc

prefix = 'silmap';

%load clusters
name = 'rest1_U_21+orig'; 
clustname= 'rest1_U_21_kmeans_g2k20+orig';

%name = 'rest1_US+orig';
%clustname = 'rest1_US_kmeans_g2k20+orig';

[err, imclust, ~] = BrikLoad(clustname,'vector');
[err, im, info, ~] = BrikLoad(name,'vector');
m = imclust ~= 0 ;
N = length(imclust)

%mask the data
mclust = imclust(m);
mim = im(m,:);

%calculate silhouette
s = silhouette(mim,mclust,'correlation');


%output silmap

outinfo.DATASET_RANK = [3 1];
outinfo.DATASET_DIMENSIONS = info.DATASET_DIMENSIONS;
outinfo.TYPESTRING = info.TYPESTRING;
outinfo.SCENE_DATA = info.SCENE_DATA;
outinfo.ORIENT_SPECIFIC = info.ORIENT_SPECIFIC;
outinfo.ORIGIN = info.ORIGIN;
outinfo.DELTA = info.DELTA;

       


out = zeros(N,1);
out(m) = s;

Opt.Prefix = prefix;
Opt.Scale = 1;
Opt.OverWrite = 'y';
WriteBrik(out,outinfo,Opt);










