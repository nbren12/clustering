function [percosts,VIs,baseline]= clusthung_linkage(varargin)


if nargin>0
    link1fn=varargin{1};
    link2fn=varargin{2};
    mask1fn=varargin{3};
    mask2fn=varargin{4};
    range=varargin{5};
else
    link1fn='r1_linkage.mat';
    link2fn='r2_linkage.mat';
    mask1fn='r1_mask+orig';
    mask2fn='r2_mask+orig';
    range=[2,5,10,25,50,75,100,150,200,250,300,350,400,450,500,750,1000,1500,2000];
end

l1 = load(link1fn);
l1 = l1.li;

l2 = load(link2fn);
l2 = l2.li;

mask1 = BrikLoad(mask1fn,'vector');
mask2 = BrikLoad(mask2fn,'vector');

percosts = [];
VIs = [];
baseline = 2*log(range);


for clnum=range
    fprintf('On K Level %d\n',clnum);
    cl1= cluster(l1,'maxclust',clnum);
    cl1_um = unmask(cl1,mask1);
    cl2= cluster(l2,'maxclust',clnum);
    cl2_um = unmask(cl2,mask2);
    M = (cl1_um~=0) & (cl2_um~=0);
    
    mv1 = cl1_um(M,:);
    mv2 = cl2_um(M,:);
    
    %compute cost matrix
%     nk = max(mv1);
    
%     costmat = -confusionmat(mv1,mv2);
    
    %do hungarian algorithm
    
%     [mapping, cost] = munkres(costmat);
    
%     percost = (sum(M)+cost)/sum(M);
    VI = varinfo(mv1,mv2);

%     percosts = [percosts percost];
    VIs = [VIs VI];
    
%     costmat = diag(costmat(1:nk,mapping));
% 
%     out = zeros(length(M),1);
%     for i =1:nk
%         out(cl2_um==mapping(i)) = i;
%     end

    % Output the remapped
    [name1_prefix, suffix] = strtok(link1fn,'_linkage');
    [name2_prefix, suffix] = strtok(link2fn,'_linkage');
    %write_afni(cl1_um,sprintf('l%i_%s_match_%s',clnum,name1_prefix,name2_prefix),mask1fn);
    %write_afni(out,sprintf('l%i_%s_match_%s',clnum,name2_prefix,name1_prefix),mask2fn);
    %system(sprintf('3drefit -redo_bstat %s+orig',sprintf('l%i_%s_match_%s',clnum,name1_prefix,name2_prefix)));
    %system(sprintf('3drefit -redo_bstat %s+orig',sprintf('l%i_%s_match_%s',clnum,name2_prefix,name1_prefix)));

end

 %outmat = [range;VIs;percosts]';
 outmat = [range;VIs]';
 save(sprintf('%s_v_%s_VI.txt',name1_prefix,name2_prefix),'outmat','-ASCII');

end

function out = unmask(data,mask)

out = zeros(length(mask),size(data,2));
out(mask~=0,:) = data;


end
