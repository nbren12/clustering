function savelinkage(maskfn,volfn,name)
    li = afni_linkage(maskfn,volfn);
    save([name,'.linkage.mat'],'li');
