function [ucpats] = patvecs(clustmax)
% start at stage 2
l = load('linkage.mat');
l=l.l;
prevc=cluster(l,'maxclust',1);
% Look up the tree
% Find which cluster split, n
% Create temporary cluster with anything > n +1 and anything < n - 1
% Pick which one is closest to the R or L neighbor
ucpats=[prevc];
for c=2:clustmax
    nextc=zeros(size(prevc));
    tempc=cluster(l,'maxclust',c);
    % find what split
    breakpt=1;
    ind=find(prevc==breakpt);
    while length(unique(tempc(ind)))==1
        breakpt=breakpt+1;
        ind=find(prevc==breakpt);
    end
    for uc=unique(tempc(ind))'
        ucind = find(tempc==uc);
        ucpat = zeros(size(prevc)); %unique pattern holder
        ucpat(ucind)=1;
        ucpats = [ucpats ucpat ];
    end
    prevc=tempc;
end
pats=ucpats;
save('pats.mat','pats');

