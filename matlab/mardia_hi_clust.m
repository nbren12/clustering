clear all
close all
clc

%load images
name1= 'rest1_U_21+orig';
mask1 = 'mask1_nowm+orig';


[im info]  = BrikLoad(name1,'vector');
m = BrikLoad(mask1,'vector'); m = (m~=0);
[N NT] = size(im);

%mask the data
mim = im(m,:);
Nm = sum(m);

%znormalize
znorm = mim-repmat(mean(mim,2),[1 NT]);
znorm = znorm./repmat(std(mim,[],2),[1 NT]);

%do linkage
Z = linkage(znorm, 'ward','euclidean');
clust = cluster(Z,'maxclust',1);
f= [];
%get new cluster
for i =1:400
fprintf('Iterarion %d\n',i);
tmpclust = cluster(Z,'maxclust',i+1);
for j =1:i
    crit = length(unique(tmpclust(clust==j)));
    if crit > 1
       f = [f mardia(mim(clust==j,:))];
       break;
    end
    
end
clust = tmpclust;
end

