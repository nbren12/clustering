function [VI,nMI] = varinfo(a,b)

N = length(a);

[P1, nk1] = probs_local(a);
[P2, nk2] = probs_local(b);
H1 = entropy_local(P1);
H2 = entropy_local(P2);

pij = P1*P2';

Pjoint = crosstab(a,b)/N;
summe = Pjoint.*log(Pjoint./pij);
summe(Pjoint==0) = 0;
MI = sum(sum(summe));
nMI = MI/(H1+H2);
 




VI = H1 + H2 - 2*MI;

end

function [P,nk] = probs_local(L)

ks = unique(L);
nk = length(ks);

N = length(L);
P  =zeros(nk,1);

for k = 1:nk
    P(k) = sum(L==ks(k))/N;
end

end

function H = entropy_local(P)

H = sum(-P.*log(P));

end

