function [mapping, percost, VI,costmat]= clusthung(name1,name2)


%load data and apply mask
opt.Format = 'vector';

[err, v1, Info1, ErrMessage] = BrikLoad (name1, opt);
[err, v2, Info2, ErrMessage] = BrikLoad (name2, opt);

V1 = v1(:,1);
V2 = v2(:,1);

M = (V1~=0) & (V2~=0);

mv1 = V1(M,:);
mv2 = V2(M,:);

%compute cost matrix
nk = max(mv1);

costmat = -confusionmat(mv1,mv2);

%do hungarian algorithm


[mapping, cost] = munkres(costmat);

percost = (sum(M)+cost)/sum(M);
VI = varinfo(mv1,mv2);


costmat = diag(costmat(1:nk,mapping));

out = zeros(length(M),1);
for i =1:nk
out(V2==mapping(i)) = i;
end

v1(:,1) = out;

% Output the remapped
Opt.Prefix = 'match';
Opt.Scale = 1;
Opt.OverWrite = 'y';
WriteBrik(v1,Info2,Opt);
