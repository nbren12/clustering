function [li,msk] = afni_linkage(mskfn,volfn)
    msk = BrikLoad(mskfn,'vector'); msk = (msk > 0 );
    vol = BrikLoad(volfn,'vector');
    mvol = vol(msk,:);
    li = linkage(mvol,'ward','euclidean');
    
    %pd = pdist(mvol,'euclidean');
    
    
    
    
