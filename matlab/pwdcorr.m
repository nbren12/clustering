function C = pwdcorr(name1,mname1,name2,mname2)
%C = pwdcorr(name1,mname1,name2,mname2)

opt.Format = 'vector';

k = 7000;

m1 = 'correlation';
method = 'bpdn_corr';



[err, V1, Info1, ErrMessage] = BrikLoad (name1, opt);
[err, V2, Info2, ErrMessage] = BrikLoad (name2, opt);
[err, M1, MInfo, ErrMessage] = BrikLoad (mname1, opt);
[err, M2, MInfo, ErrMessage] = BrikLoad (mname2, opt);

M = logical(M1)& logical(M2) & (std(V1')'~=0) & (std(V2')' ~= 0);
n = sum(M);
inds = randsample(n,k,false);
%inds = 1:n;
%inds =5:100:n;

mv1 = V1(M,:); mv1 = mv1(inds,:);
mv2 = V2(M,:); mv2 = mv2(inds,:);





tic
fprintf('Calculating Pdists\n');
p1 = pdist(mv1,m1);
p2 = pdist(mv2,m1);

fprintf('Normalizing\n');
%p1 = p1 - mean(p1); p1 = p1/norm(p1);
%p2 = p2 - mean(p2); p2 = p2/norm(p2);

fprintf('Calculating Correlation\n');
C = corr(p1(:),p2(:));

fprintf('Writing File\n');
f= fopen('corr.txt','a');
fprintf(f,'%s %s %s %s %s %e\n',method,name1,name2,mname1,mname2,C);
fclose(f);
toc
