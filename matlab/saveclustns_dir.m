function [err] = saveclustns_dir(brikname,linkage,range,prefix,maskbrik,dirmaskbrik)
mask_vec=BrikLoad(maskbrik,'vector');
dirmask_vec=BrikLoad(dirmaskbrik,'vector');
disp('Loading volume and mask..')
brik=BrikLoad(brikname,'vector');
brik=mask(brik,mask_vec);
dirmask=mask(dirmask_vec,mask_vec);

%Select seed cluster
dirseedclust=cluster(linkage,'maxclust',200);
dirseed=dirseedclust(find(dirmask~=0));
unqlabs=unique(dirseed)';
sellab = -1;
maxvox = 0;
for lab=unique(unqlabs)
    thisvox=size(find(dirseed==lab),1) ;
    if thisvox>maxvox
        sellab = lab;
        maxvox=thisvox;
    end
end
dirseed=zeros(size(mask_vec));
dirseed(find(dirseedclust==sellab))=1;

top=max(range);

% start at stage 2
prevc=cluster(linkage,'maxclust',1);

clustnss = prevc

% Look up the tree
% Find which cluster split, n
% Create temporary cluster with anything > n +1 and anything < n - 1
% Pick which one is closest to the R or L neighbor

for c=2:top
    nextc=zeros(size(prevc));
    tempc=cluster(linkage,'maxclust',c);
    % find what split
    breakpt=1;
    ind=find(prevc==breakpt);
    while length(unique(tempc(ind)))==1
        breakpt=breakpt+1;
        ind=find(prevc==breakpt);
    end
    % so now we know cluster breakpt split in previous one
    % anything to the right + 1 and keep values to left
    nextc(find(prevc>breakpt))=prevc(find(prevc>breakpt))+1;
    nextc(find(prevc<breakpt))=prevc(find(prevc<breakpt));
    
    % now it's prepared for next insertion, gotta determine which one is
    % closest to its neighbor in feature space (brik)
    ln=mean(brik(find(prevc==(breakpt-1)),:),1);
    rn=mean(brik(find(prevc==(breakpt+1)),:),1);
    dirn=mean(brik(find(dirseed~=0),:),1);
    a=unique(tempc(ind));
    nc1=mean(brik(find(tempc==a(1)),:),1);
    nc2=mean(brik(find(tempc==a(2)),:),1);
    
    % check distances
    if breakpt==1 && breakpt==(c-1) %start of tree, no left or right neighbors, introduce direction
        dmatr=dist([nc1;nc2;zeros(size(nc1))*10000;dirn]'); 
    elseif breakpt==(c-1) % no right neighbors
        dmatr=dist([nc1;nc2;ln;dirn]');         
    elseif breakpt==1 % no left neighbors
        dmatr=dist([nc1;nc2;zeros(size(nc1))*10000;rn]');    
    else    
        dmatr=dist([nc1;nc2;ln;rn]');
    end
        
    if dmatr(1,3)<=dmatr(2,3) || dmatr(1,4)>=dmatr(2,4) 
    % that is, if first is closer to the left or farther from the right
        nextc(find(tempc==a(1)))=breakpt;
        nextc(find(tempc==a(2)))=breakpt+1;
    else
        nextc(find(tempc==a(1)))=breakpt+1;
        nextc(find(tempc==a(2)))=breakpt;
    end
    if find(range==c)
        tempc=unmask(nextc,mask_vec);
        write_afni(tempc,sprintf('%s%04d',prefix,c),maskbrik);
        disp(sprintf('Writing cluster %d',c));
    else
        disp(sprintf('Calculating but not saving %d',c));
    end
    clustnss = [clustnss nextc];
    prevc=nextc;
end
save('clustns.mat','clustnss')
disp('Concatening clusters into 4D file..')
system(sprintf('3dTcat -overwrite -prefix %s %s????+orig.BRIK',prefix,prefix));
system(sprintf('rm -f %s????+orig.*',prefix)); 
disp('Finished!');

